package buu.satja.math

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnAdd = findViewById<Button>(R.id.btnAdd)
        btnAdd.setOnClickListener{
            val intent = Intent(MainActivity@this, AddActivity::class.java)
            startActivity(intent)
        }
        val btnMinus = findViewById<Button>(R.id.btnMinus)
        btnMinus.setOnClickListener{
            val intent = Intent(MainActivity@this, MinusActivity::class.java)
            startActivity(intent)
        }
        val btnMultiply = findViewById<Button>(R.id.btnMultiply)
        btnMultiply.setOnClickListener{
            val intent = Intent(MainActivity@this, MultiplyActivity::class.java)
            startActivity(intent)
        }

    }

}