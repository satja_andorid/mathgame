package buu.satja.math

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
var correct: Int = 0
var incorrect: Int = 0
class AddActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add)
        play()

    }

    fun play() {
        val btnBack = findViewById<Button>(R.id.btnBack)
        val btnReset = findViewById<Button>(R.id.btnReset)

        val Num1 = findViewById<TextView>(R.id.num1)
        val Num2 = findViewById<TextView>(R.id.num2)
        val btnAns1 = findViewById<Button>(R.id.btnAns1)
        val btnAns2 = findViewById<Button>(R.id.btnAns2)
        val btnAns3 = findViewById<Button>(R.id.btnAns3)
        val txtShow = findViewById<TextView>(R.id.txtShowAns)
        val txtStatic = findViewById<TextView>(R.id.txttrue)
        val btnPlay = findViewById<Button>(R.id.btnAgain)
        btnAns1.setBackgroundColor(Color.GRAY)
        btnAns2.setBackgroundColor(Color.GRAY)
        btnAns3.setBackgroundColor(Color.GRAY)
        var isSelectAnswer = false
        val ran1 = (0..9).random()
        val ran2 = (0..9).random()

        val result = ran1 + ran2

        Num1.setText(ran1.toString())
        Num2.setText(ran2.toString())
        val result1 = result+(1..5).random()
        var result2 = result+(1..5).random()
        while (true){
            if (result1 == result2){
                result2 = result+(1..5).random()
            }else{
                break;
            }
        }

        val c = (1..3).random()
        if (c==1){
            btnAns1.text = result.toString()
            btnAns2.text = result1.toString()
            btnAns3.text = result2.toString()

        }
        if (c==2){
            btnAns2.text = result.toString()
            btnAns3.text = result1.toString()
            btnAns1.text = result2.toString()
        }
        if (c==3){
            btnAns3.text = result.toString()
            btnAns1.text = result1.toString()
            btnAns2.text = result2.toString()
        }
        btnAns1.setOnClickListener {
            if (c == 1) {
                if (!isSelectAnswer){
                    btnAns1.setBackgroundColor(Color.GREEN)
                    txtShow.text = "CORRECT"
                    correct++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    isSelectAnswer = true
                }
            } else {
                if (!isSelectAnswer){
                    btnAns1.setBackgroundColor(Color.RED)
                    txtShow.text = "INCORRECT"
                    incorrect++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    isSelectAnswer = true
                }
            }
        }
        btnAns2.setOnClickListener {
            if (c == 2) {
                if (!isSelectAnswer){
                    btnAns2.setBackgroundColor(Color.GREEN)
                    txtShow.text = "CORRECT"
                    correct++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    isSelectAnswer = true
                }
            } else {
                if (!isSelectAnswer){
                    btnAns2.setBackgroundColor(Color.RED)
                    txtShow.text = "INCORRECT"
                    incorrect++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    isSelectAnswer = true
                }
            }
        }
        btnAns3.setOnClickListener {
            if (c == 3) {
                if (!isSelectAnswer){
                    btnAns3.setBackgroundColor(Color.GREEN)
                    txtShow.text = "CORRECT"
                    correct++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    isSelectAnswer = true
                }
            } else {
                if (!isSelectAnswer){
                    btnAns3.setBackgroundColor(Color.RED)
                    txtShow.text = "INCORRECT"
                    incorrect++
                    txtStatic.text = "correct:${correct} incorrect:${incorrect}"
                    isSelectAnswer = true
                }
            }
        }
        btnPlay.setOnClickListener {
            play()
        }
        btnBack.setOnClickListener{
            correct = 0;
            incorrect = 0;
            onBackPressed()
        }
        btnReset.setOnClickListener {
            correct = 0;
            incorrect = 0;
            play()
            txtStatic.text = "correct:${correct} incorrect:${incorrect}"
        }

    }

}